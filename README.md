# Implify
A Java HTTP server.

## Download
You can find Implify in the Maven Central Repository:
```
<dependency>
    <groupId>net.scrumplex</groupId>
    <assemblyId>implify</assemblyId>
    <version>0.0.1</version>
</dependency>
```
## Usage
You can find examples [here](https://github.com/Scrumplex/Implify/tree/master/examples) and for further documentation refer to the [wiki](https://github.com/Scrumplex/Implify/wiki).

## License
This project is licensed under the Apache License 2.0. You can find more information in the *NOTICE* file